# springcloud-template

#### 项目介绍

springcloud微服务架构模板

#### 软件架构
![image](./doc/images/spring-cloud-template.png)

#### 技术栈
![image](./doc/images/技术栈.png)

#### 模块说明

- api-common 公共包 
- api-eureka 注册中心 
- api-model 实体类
- api-oauth 认证授权
- api-raise 众筹服务
- api-ticket 票务服务
- api-user 用户服务
- api-zipkin 链路跟踪
- api-zuul 网关服务
- elastic-job 分布式任务
- transaction-springcloud 分布式事务核心模块
- tx-manager 事务管理器
- doc 文档图片
- z-deploy 基础软件架构部署文档教程

> 搜索模块原先采用solr，考虑到es更加贴合所以暂时去除api-search模块

> elastic-job 后面改成本人自行改造的xxl-job-discovery 参考链接 [xxl-job-discovery]()

#### 参与贡献
> 想参与共建模块请联系 微信 `MOHN581`



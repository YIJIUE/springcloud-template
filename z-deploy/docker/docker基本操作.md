#### 拉取/推送/搜索镜像
```
docker pull nginx / docker push nginx / docker search nginx
```
#### 创建docker网段
```
docker network create api
```
#### 运行nginx镜像 
> d表示后台运行 restart表示容器停止时是否重启 name表示为容器取的名称 net表示处于哪个网段 
 v表示将容器里的data目录挂载到宿主机/data/nginx目录 -p表示讲容器端口80映射到宿主机端口8080 最后一个拉取的容器镜像
```
docker run -d --restart=always --name=nginx --net=api --privileged=true -v /data/nginx:/data -p 8080:80 -p 443:443 nginx
```
#### 删除镜像
```
docker rmi dsfasf123(容器id)   / docker rm -f nginx （强制删除）
```
#### 运行一个带ping的容器 用于测试是否通过名称能连接其它容器
```
docker run -d --restart=always --name=bootcamp --net=api docker.io/ynouiara/kubernetes-bootcamp-requester
```
#### 进入容器内部
```
docker exec -it nginx sh / bash 
```
#### 查看容器日志
```
docker logs nginx
```
#### 查看容器网络配置
```
docker network inspect api
```
#### 创建docker私有仓库
```
docker run -d -v /data/registry:/var/lib/registry -p 5000:5000 --restart=always --privileged=true --name registry registry:latest
```
#### 重新打标签
```
docker tag nginx localhost:5000/nginx
```
#### 推送镜像到私有仓库
```
docker push localhost:5000/nginx
```
#### 将容器nginx连接到api网络中
```
docker network connect api nginx
```
#### 分配合理资源内存 oom可以防止被宿主机杀死
```
docker run -d --name=nginx --oom-kill-disable nginx
```
#### dockerfile构建镜像
```
docker build -t tomcat .
```